<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Daftar route untuk tampilan user biasa!
|
*/

// Route::get('/', function () {
// return view('welcome');
// });

Route::get('/', function () {
    return view('Landingpage', ["title" => "Komunitas Olahraga Wind Junior"]);
})->name('landingpage');

Route::get('/home', function () {
    return view('Home', ["title" => "Komunitas Olahraga Wind Junior"]);
})->name('home');

Route::get('/news', function () {
    return view('Berita', ["title" => "Berita Seputar Wind Junior"]);
})->name('berita');

Route::get('/newspaper/news/detail', function () {
    return view('Detail_berita', ["title" => "Berita Seputar Wind Junior"]);
})->name('detailberita');

Route::get('/login', function () {
    return view('login_admin', ["title" => "Masuk"]);
})->name('login_admin');

Route::get('/manajemen', function () {
    return view('Manajemen', ["title" => "Kepengurusan"]);
})->name('manajemen');

Route::get('/newspaper/match', function () {
    return view('Match', ["title" => "Pertandingan Wind Junior"]);
})->name('match');

Route::get('/newspaper/pendaftaran', function () {
    return view('Pendaftaran', ["title" => "Pendaftaran Wind junior"]);
})->name('pendaftaran');

Route::get('/divisi', function () {
    return view('Team', ["title" => "Divisi wind junior"]);
})->name('divisi');

Route::get('/newspaper/prestasi', function () {
    return view('Prestasi', ["title" => "Divisi wind junior"]);
})->name('prestasi');






/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Daftar route untuk tampilan user Admin!
|
*/

Route::get('/admin', function () {
    return view('admin_login', ["title" => "Login"]);
})->name('login_admin');

Route::get('/admin/dashboard', function () {
    return view('admin/Admin_admin', ["title" => 'Dashboard']);
})->name('dashboard_admin');

Route::get('/admin/profile', function () {
    return view('admin/Admin_profile', ["title" => 'Profile']);
})->name('profile_admin');

Route::get('/admin/anggota', function () {
    return view('admin/Admin_anggota', ["title" => "Anggota"]);
})->name('admin_anggota');

Route::get('/admin/berita', function () {
    return view('admin/Admin_berita', ["title" => "Berita"]);
})->name('admin_berita');

Route::get('/admin/divisi', function () {
    return view('admin/Admin_divisi', ["title" => "Divisi"]);
})->name('admin_divisi');

Route::get('/admin/kegiatan', function () {
    return view('admin/Admin_kegiatan', ["title" => "Kegiatan"]);
})->name('admin_kegiatan');

Route::get('/admin/pelatih', function () {
    return view('admin/Admin_pelatih', ["title" => "Pelatih"]);
})->name('admin_pelatih');

Route::get('/admin/pelatih', function () {
    return view('admin/Admin_pelatih', ["title" => "Pelatih"]);
})->name('admin_pelatih');


/*
|--------------------------------------------------------------------------
| Form Routes
|--------------------------------------------------------------------------
|
| Daftar route untuk tampilan form user Admin!
|
*/

/*
|--------------------------------------------------------------------------
| Admin Form Routes
|--------------------------------------------------------------------------
*/

Route::get('/admin/form/user/create', function () {
    return view('admin/form/form_admin-create', ["title" => "Buat User Admin"]);
})->name('form_admin-create');

Route::get('/admin/form/user/edit/{id_user}', function () {
    return view('admin/form/form_admin-edit', ["title" => "Edit User Admin"]);
})->name('form_admin-edit');

Route::get('/admin/form/user/change', function () {
    return view('admin/form/form_admin-change', ["title" => "Edit Password"]);
})->name('form_admin-change');


/*
|--------------------------------------------------------------------------
| Anggota Form Routes
|--------------------------------------------------------------------------
*/

Route::get('/admin/form/anggota/create', function () {
    return view('admin/form/form_anggota', ["title" => "Buat Anggota"]);
})->name('form_anggota-create');

Route::get('/admin/form/anggota/edit/{id}', function () {
    return view('admin/form/form_anggota', ["title" => "Edit Anggota"]);
})->name('form_anggota-edit');

/*
|--------------------------------------------------------------------------
| Berita Form Routes
|--------------------------------------------------------------------------
*/


Route::get('/admin/form/berita/create', function () {
    return view('admin/form/form_berita', ["title" => "Buat Berita"]);
})->name('form_berita-create');

Route::get('/admin/form/berita/edit/{id_berita}', function () {
    return view('admin/form/form_berita', ["title" => "Edit Berita"]);
})->name('form_berita-edit');

/*
|--------------------------------------------------------------------------
| Divisi Form Routes
|--------------------------------------------------------------------------
*/


Route::get('/admin/form/divisi/create', function () {
    return view('admin/form/form_divisi', ["title" => "Buat Divisi"]);
})->name('form_divisi-create');

Route::get('/admin/form/divisi/edit/{id}', function () {
    return view('admin/form/form_divisi', ["title" => "Edit Divisi"]);
})->name('form_divisi-edit');

/*
|--------------------------------------------------------------------------
| Kegiatan Form Routes
|--------------------------------------------------------------------------
*/


Route::get('/admin/form/kegiatan/create', function () {
    return view('admin/form/form_kegiatan', ["title" => "Buat Kegiatan"]);
})->name('form_kegiatan-create');

Route::get('/admin/form/kegiatan/edit/{id}', function () {
    return view('admin/form/form_kegiatan', ["title" => "Edit Kegiatan"]);
})->name('form_kegiatan-edit');

/*
|--------------------------------------------------------------------------
| Pelatih Form Routes
|--------------------------------------------------------------------------
*/


Route::get('/admin/form/pelatih/create', function () {
    return view('admin/form/form_pelatih', ["title" => "Buat Pelatih"]);
})->name('form_pelatih-create');

Route::get('/admin/form/pelatih/edit/{id}', function () {
    return view('admin/form/form_pelatih', ["title" => "Edit Pelatih"]);
})->name('form_pelatih-edit');