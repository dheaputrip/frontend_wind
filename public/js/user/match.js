$.ajax ({
    method: "GET",
    url: baseURL + "/api/kegiatan/get/match",
}).done(function(response){
    console.log(response);

    let data_esport = response.result.esport;
    $('#tanggal-mulai').text(data_esport.tanggal_selesai);
    $('#lokasi').text(data_esport.tempat);
    $('#tim-lawan').text(data_esport.klub_lawan);
    $('#skor-lawan').text("-");
    $('#skor-kandang').text("-");


    let data_u19 = response.result.data_u19;
    $('#tanggal-mulai').text(data_u19.tanggal_selesai);
    $('#lokasi').text(data_u19.tempat);
    $('#tim-lawan').text(data_u19.klub_lawan);
    $('#skor-lawan').text("-");
    $('#skor-kandang').text("-");

    
    
}).fail(function(error){
    console.log(error);
})

$('#status-esport').on('change', function(){
    let status = $(this).val();
    $.ajax ({
        method: "GET",
        url: baseURL + "/api/kegiatan/get/esport/status/"+status,
    }).done(function(response){
        console.log(response);
        let data = response.result;
        $('#tanggal-mulai').text(data.tanggal_selesai);
        $('#lokasi').text(data.tempat);
        $('#tim-lawan').text(data.klub_lawan);

        if(status=="belum_selesai"){
            $('#skor-lawan').text("-");
            $('#skor-kandang').text("-");
        } 
        
        else {
            $('#skor-lawan').text(data.skor_tandang);
            $('#skor-kandang').text(data.skor_kandang);
        }
    
    }).fail(function(error){
        console.log(error);
    })
})