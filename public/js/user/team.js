function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
$.ajax({
        method: "GET",
        url: baseURL + "/api/divisi/get/lengkap/id/1"
    })
    .done(function (response) {
        // console.log(response);
        // let data = response.result;
        let anggota = response.result.anggota;
        let pelatih = response.result.pelatih;
        let official = response.result.official;
        // let jumlah_pemain = anggota.length;
        // console.log(jumlah_pemain);
        // console.log(pelatih.length);
        // console.log(official.length);
        // $("#nama-team").text(data.divisi[0].nama);
        //Pilih pemain
        // $(".pilih-pemain").empty().append("<option value=0 selected>Pilih Pemain</option>");
        anggota.forEach(element => {
            $(".pilih-pemain").append("<option value=" + element.id + ">" + element.nama + "</option>");
        });
        var id_pemain = anggota[0].id;
        $.ajax({
            method: 'GET',
            url: baseURL + '/api/anggota/get/id/' + id_pemain,
        }).done(response => {
            console.log(response);
            let pemain = response.result;
            $('.nama-pemain').text(pemain.nama);
            if (pemain.id_divisi > 1) {
                $('tr.nomor-punggung').text(pemain.nomor_punggung);
                $('tr.nomor-punggung').removeClass('d-none').addClass('d-block');
            } else {
                $('tr.nomor-punggung').removeClass('d-block').addClass('d-none');
            }
            $('.umur-pemain').text(getAge(pemain.tanggal_lahir) + ' Tahun');
            $('.jk-pemain').text(pemain.jenis_kelamin == "L" ? 'Laki-laki' : 'Perempuan');
            $('.tinggi-pemain').text(pemain.tinggi_badan + ' cm');
            $('.berat-pemain').text(pemain.berat_badan + ' kg');
            $('.posisi-pemain').text(pemain.posisi);
            $('.divisi-pemain').text(pemain.id_divisi > 1 ? "Posisi" : "Role");
        })
        //Pilih Pelatih
        // $(".pilih-pelatih").empty().append("<option value=0 selected>Pilih Pelatih</option>");
        pelatih.forEach(element => {
            $(".pilih-pelatih").append("<option value=" + element.id + ">" + element.nama + "</option>");
        });

        //Pilih Official
        // $(".pilih-official").empty().append("<option value=0 selected>Pilih Official</option>");
        official.forEach(element => {
            $(".pilih-official").append("<option value=" + element.id + ">" + element.nama + "</option>");
        });
    })
    .fail(function (error) {
        console.log(error);
    });

$("#nav-tab").on('click', 'a.nav-item', (function () {
    // var delete_id = $(this).data('id');
    let id_divisi = $(this).data('id'); //deklarasi variable
    console.log(id_divisi);
    $.ajax({
            method: "GET",
            url: baseURL + "/api/divisi/get/lengkap/id/" + id_divisi
        })
        .done(function (response) {
            console.log(response);
            let data = response.result;
            let anggota = data.anggota;
            let pelatih = data.pelatih;
            let official = data.official;
            let jumlah_pemain = anggota.length;
            console.log(jumlah_pemain);
            console.log(pelatih.length);
            console.log(official.length);
            // $("#nama-team").text(data.divisi[0].nama);
            //Pilih pemain
            $("select.pilih-pemain").empty().append("<option value=0 selected>Pilih Pemain</option>");
            anggota.forEach(element => {
                $("select.pilih-pemain").append("<option value=" + element.id + ">" + element.nama + "</option>");
            });

            //Pilih Pelatih
            $(".pilih-pelatih").empty().append("<option value=0 selected>Pilih Pelatih</option>");
            pelatih.forEach(element => {
                $(".pilih-pelatih").append("<option value=" + element.id + ">" + element.nama + "</option>");
            });

            //Pilih Official
            $(".pilih-official").empty().append("<option value=0 selected>Pilih Official</option>");
            official.forEach(element => {
                $(".pilih-official").append("<option value=" + element.id + ">" + element.nama + "</option>");
            });
        })
        .fail(function (error) {
            console.log(error);
        });
}));

$("select.pilih-pemain").on('change', function () {
    let id_pemain = $(this).val();
    $.ajax({
        method: 'GET',
        url: baseURL + '/api/anggota/get/id/' + id_pemain,
    }).done(response => {
        let pemain = response.result;
        $('.nama-pemain').text(pemain.nama);
        $('.nomor-punggung').text(pemain.nomor_punggung);
    })
})
