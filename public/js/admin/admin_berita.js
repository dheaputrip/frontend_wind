if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});

$.ajax({
    method: 'GET',
    url: baseURL + '/api/post/get/all',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    if (response.code === 200) {
        $('#tableBerita').DataTable({
            data: response.result,
            columns: [{
                    data: 'penulis'
                },
                {
                    data: 'judul'
                },
                {
                    data: 'status'
                },
                {
                    "render": function (data, type, full) {
                        return '<a class="btn btn-warning btn-sm" href="' + vBaseURL + '/admin/form/berita/edit/' + full.id + '">' + 'Edit' +
                            '</a>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        return '<a class="btn btn-danger btn-sm hapus" data-id="' + full.id + '" >Hapus</a>';
                    }
                }
            ]
        })
    }
}).fail(function () {
    console.log(error);
})

$('#tableBerita').on('click', 'a.hapus', function () {
    var delete_id = $(this).data('id');
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                method: 'DELETE',
                url: baseURL + '/api/post/delete/id/' + delete_id,
                headers: {
                    Authorization: 'Bearer' + Cookies.get('nubmaster69')
                }
            }).done(function (response) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                window.location.reload();
            }).fail(function (error) {
                console.log(error);
            })
        }
    })
})

$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
