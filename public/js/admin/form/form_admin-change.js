//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
});

$('#logout').click(function () {
    $.ajax({
        method: 'POST',
        url: baseURL + '/api/auth/logout',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('nubmaster69')
        }
    })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})

$('#ubahPassword').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    let data = form.serialize();

    $.ajax({
        method: 'POST',
        url: baseURL + '/api/user/change',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('nubmaster69')
        },
        data,
    })
        .done(function (response) {
            if (response.code == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil ubah password',
                    timer: 1500,
                })
                Cookies.remove('nubmaster69');
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            } else {
                Swal.fire({
                    type: 'Failed',
                    title: 'Gagal ubah password',
                    timer: 1500,
                })
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        })
        .fail(function (error) {
            error = error.responseJSON;
            console.log(error);
            let err_msg = error.message;
            if (error.message.hasOwnProperty('password')) {
                // err_msg = error.message.username[0] || error.message.password[0];
                err_msg = error.message.password[0];
                // Swal.fire({
                //     type: 'error',
                //     title: 'Oops...',
                //     text: err_msg,
                //     //footer: '<a href>Why do I have this issue?</a>'
                // })
            } else if (error.message.hasOwnProperty('new_password')) {
                err_msg = error.message.new_password[0];
            }

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: err_msg,
                //footer: '<a href>Why do I have this issue?</a>'
            })
        })
})
