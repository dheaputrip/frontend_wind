//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$('#register').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    let data = form.serialize();
    //console.log(data);
    $.ajax({
        method: 'POST',
        url: baseURL + '/api/user/register',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('nubmaster69')
        },
        data,
    })
        .done(function (response) {
            // console.response;
            // console.log(response);
            // Cookies.set('nubmaster69', response.token, { expires: 1 });
            // alert(response.message);
            if (response.code == 201) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil Mendaftar!',
                    //text: 'Something went wrong!',
                    timer: 1500,
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/dashboard";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Username sudah digunakan!',
                    //text: 'Something went wrong!',
                    timer: 1500,
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        })
        .fail(function (error) {
            error = error.responseJSON;
            console.log(error);
            let err_msg;
            if (error.message.hasOwnProperty('password')) {
                // err_msg = error.message.username[0] || error.message.password[0];
                err_msg = error.message.password[0];
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: err_msg,
                    //footer: '<a href>Why do I have this issue?</a>'
                })
            } else {
                err_msg = error.message.username[0];
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: err_msg,
                    //footer: '<a href>Why do I have this issue?</a>'
                })
            }
        })
});

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});

$('#logout').click(function () {
    $.ajax({
        method: 'POST',
        url: baseURL + '/api/auth/logout',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('nubmaster69')
        }
    })
        .done(function (response) {
            window.location.reload();
            Cookies.remove('nubmaster69');
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
