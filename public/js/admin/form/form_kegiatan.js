//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});

var url = document.URL;
var id = url.substring(url.lastIndexOf('/') + 1);
console.log(url);
console.log(id);

//membedakan mana edit mana create
if (id >= 0) {
    $.ajax({
        method: 'GET',
        url: baseURL + '/api/kegiatan/get/id/' + id,
        headers: {
            Authorization: 'Bearer ' + Cookies.get('nubmaster69')
        }
    }).done(function (response) {
        let data = response.result;
        console.log(data);
        $('#selectDivisi').val(data.id_divisi).attr('selected');
        $('#inputName').val(data.nama);
        $('#selectJenis').val(data.jenis);
        $('#inputTempat').val(data.tempat);
        $('#inputKlub').val(data.klub_lawan);
        $('#inputSkorKandang').val(data.skor_kandang);
        $('#inputSkorTandang').val(data.skor_tandang);
        $('#selectStatus').val(data.status).attr('selected');
        $('#inputTanggalMulai').val(data.tanggal_mulai);
        $('#inputJamMulai').val(data.jam_mulai);
        $('#inputTanggalSelesai').val(data.tanggal_selesai);
        $('#inputJamSelesai').val(data.jam_selesai);
        console.log(response);
    }).fail(function (error) {
        console.log(error);
        console.log(error.responseJSON.message);
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'PUT',
            url: baseURL + '/api/kegiatan/edit/id/' + id,
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil diedit',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/kegiatan";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
} else {
    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'POST',
            url: baseURL + '/api/kegiatan/add',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 201) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil ditambahkan',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/kegiatan";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
}

$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
