//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});

var url = document.URL;
var id = url.substring(url.lastIndexOf('/') + 1);
console.log(url);
console.log(id);

if (id >= 0) {
    $.ajax({
        method: 'GET',
        url: baseURL + '/api/divisi/get/id/' + id,
        headers: {
            Authorization: 'Bearer' + Cookies.get('nubmaster69')
        }
    }).done(function (response) {
        let data = response.result;
        console.log(data);
        $('#inputNamaDivisi').val(data.nama);
        $('#inputJumlahAnggota').val(data.jumlah_anggota);
    }).fail(function (error) {
        console.log(error.message);
    })

    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'PUT',
            url: baseURL + '/api/divisi/edit/id/' + id,
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil diedit',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/divisi";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
} else {
    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'POST',
            url: baseURL + '/api/divisi/add',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 201) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil ditambahkan',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/divisi";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
}


$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
