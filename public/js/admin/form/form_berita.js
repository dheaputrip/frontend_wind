//Froala WYSIWYG Editor
var editor = new FroalaEditor('#text-tool');

//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/post/add',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69'),
    }
}).done(function (response) {
    if (response.code == 409) {
        console.log('woi');
        console.log(response.message);
        console.log(response);
    } else {
        // console.log('woi');
        console.log(response.message);
        console.log(response);
    }
}).fail(function (error) {
    console.log(error);
});

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
});

$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
