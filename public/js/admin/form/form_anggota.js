//Periksa apakah user sudah login
if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookie.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});

var url = document.URL;
var id = url.substring(url.lastIndexOf('/') + 1);
console.log(url);
console.log(id);

if (id >= 0) {
    $.ajax({
        method: 'GET',
        url: baseURL + '/api/anggota/get/id/' + id,
        headers: {
            Authorization: 'Bearer' + Cookies.get('nubmaster69')
        }
    }).done(function (response) {
        let data = response.result;
        console.log(data);
        $('#inputName').val(data.nama);
        $('#selectDivisi').val(data.id_divisi).attr('selected');
        $('#inputNoPunggung').val(data.nomor_punggung);
        $('#inputTempatLahir').val(data.tempat_lahir);
        $('#inputTanggalLahir').val(data.tanggal_lahir);
        $('#selectJenis').val(data.jenis_kelamin).attr('selected');
        $('#selectDarah').val(data.golongan_darah).attr('selected');
        $('#inputTinggi').val(data.tinggi_badan);
        $('#inputBerat').val(data.berat_badan);
        $('#inputAnak').val(data.anak_ke);
        $('#inputAlamat').val(data.alamat);
        $('#inputNomor').val(data.nomor_hp);
        $('#inputAsal').val(data.asal_sekolah);
        $('#inputOrtu').val(data.nama_ortu);
        $('#inputAlamatOrtu').val(data.alamat_ortu);
        $('#inputFoto').val(data.foto);
        $('#inputHpOrtu').val(data.nomor_hp_ortu);
        $('#inputKerja').val(data.pekerjaan);
        $('#selectPenghasilan').val(data.penghasilan).attr('selected');
        $('#selectAnggota').val(data.status_anggota).attr('selected');
    }).fail(function (error) {
        console.log(error.message);
    })

    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'PUT',
            url: baseURL + '/api/anggota/edit/id/' + id,
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil diedit',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/anggota";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
} else {
    $('#form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serializeArray();
        console.log(data);
        $.ajax({
            method: 'POST',
            url: baseURL + '/api/anggota/add',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            },
            data,
        }).done(function (response) {
            if (response.code == 201) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil ditambahkan',
                    timer: 1500
                })
                setTimeout(function () {
                    window.location.href = vBaseURL + "/admin/anggota";
                }, 1200);
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    //text: 'Something went wrong!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
                // setTimeout(function () {
                //     window.location.reload();
                // }, 1200);
            }
        }).fail(function (error) {
            error = error.responseJSON;
            console.log(error);
        })
    })
}

$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            window.location.reload();
            Cookies.remove('nubmaster69');
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
