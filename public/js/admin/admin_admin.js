if (!Cookies.get('nubmaster69')) {
    window.location.href = vBaseURL + "/admin";
}

$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let nama = response.result.username;
    let panjang_nama = nama.length;
    let huruf_awal = nama.substr(0, 1);
    let nama_baru = huruf_awal.toUpperCase() + nama.substr(1, panjang_nama);
    $('#nama_user').text(nama_baru);
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
});

$.ajax({
    method: 'GET',
    url: baseURL + '/api/user/get/all',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    if (response.code === 200) {
        $('#tableAdmin').DataTable({
            data: response.result,
            columns: [{
                    data: 'username'
                },
                {
                    data: 'role'
                },
                {
                    "render": function (data, type, full) {
                        return '<a class="btn btn-warning btn-sm pr-2" href="' + vBaseURL + '/admin/form/user/edit/' + full.id + '">' + 'Edit' +
                            '</a>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        return '<a class="btn btn-danger btn-sm hapus" data-id="' + full.id + '" href="#">Hapus' + '</a>';;
                    }
                }
            ]
        })
    }
}).fail(function (error) {
    console.log(error);
})

// $('#tableAdmin').on('click', 'a', function () {
//     alert('Tombol delete berhasil didengar');
// })

$('#tableAdmin').on('click', 'a.hapus', function () {
    var delete_id = $(this).data('id');
    console.log(delete_id);

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                method: 'DELETE',
                url: baseURL + '/api/user/delete/id/' + delete_id,
                headers: {
                    Authorization: 'Bearer' + Cookies.get('nubmaster69')
                }
            }).done(function (response) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                window.location.reload();
            }).fail(function (error) {
                console.log(error);
            })
        }
    })
})

$('#logout').click(function () {
    $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/logout',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('nubmaster69')
            }
        })
        .done(function (response) {
            Cookies.remove('nubmaster69');
            window.location.href = vBaseURL + "/admin";
        })
        .fail(function (error) {
            alert(error.message);
            console.log(error);
        })
})
