$.ajax({
    method: 'POST',
    url: baseURL + '/api/auth/me',
    headers: {
        Authorization: 'Bearer ' + Cookies.get('nubmaster69')
    }
}).done(function (response) {
    let user = response.result;
    if (user.role == 'super_admin') {
        $('#dashboard').removeClass('d-none').addClass('d-block');
        $('#profile').removeClass('d-block').addClass('d-none');
    } else {
        $('#profile').removeClass('d-none').addClass('d-block');
        $('#dashboard').removeClass('d-block').addClass('d-none');
    }
}).fail(function (error) {
    Cookies.remove('nubmaster69');
    window.location.href = vBaseURL + "/admin";
    console.log(error.message);
});
