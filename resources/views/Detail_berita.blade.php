<!DOCTYPE html>
@include('templates.head')
@include('templates.header')
      <div class="detail-berita">
        <h1> Wind junior menyabet penghargaan sebagai best team</h1>
        <hr>

        <div class="row">
            <div class="col-sm-8 isi-berita">
              <img src="image/futsal.jpg">
               <div class="berita-full">
                  Persib Bandung dijadwalkan bakal bertandang ke Stadion Gelora Bangkalan, markas Madura United, pada pekan ke-22 Liga 1 2019, Sabtu (5/10/2019). Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren positif melawan Madura United yang menghuni papan atas klasemen Liga 1 2019.

                  Meski bertindak sebagai Tim tuan rumah, Persib sedikit diunggulkan untuk meraih hasil positif pada laga ini. Sebab, menghadapi Madura United yang bertabur bintang, Persib memiliki tiga modal positif.
                  
                  Kemenangan melawan Madura United bisa mengangkat posisi Persib di tabel klasemen sementara Liga 1 2019. Saat ini Tim arahan Robert Rene Alberts itu masih berada di posisi ke-11 klasemen sementara dengan 24 poin dari 20 pertandingan.
                                    
               </div>
            </div>
            <!----Iklan kecil-->
            <div class="col-sm-4 iklan">
              <h5>Recent Post</h5>
              <hr>
              <!--iklan-->
              <div class="row">
                  <div class="col-sm-4 iklan-berita">
                    <img src="image/esport.jpg">
                  </div>
                  <div class="col-sm-7 iklan-berita">
                    <a href="#">Wind junior menyabet penghargaan sebagai best team</a>
                    <div class="small-iklan"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                        positif...</div> <p class="more-info" style="font-size: 1em;"> <a href="#">More Info..</a></p>
                  </div>

                  <div class="col-sm-4 iklan-berita">
                    <img src="image/esport.jpg">
                  </div>
                  
                  <div class="col-sm-7 iklan-berita">
                    <a href="#">Wind junior menyabet penghargaan sebagai best team</a>
                    <div class="small-iklan"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                        positif...</div> <p class="more-info" style="font-size: 1em;"> <a href="#">More Info..</a></p>
                  </div>

                  <div class="col-sm-4 iklan-berita">
                    <img src="image/esport.jpg">
                  </div>
                  <div class="col-sm-7 iklan-berita">
                    <a href="#">Wind junior menyabet penghargaan sebagai best team</a>
                    <div class="small-iklan"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                        positif...</div> <p class="more-info" style="font-size: 1em;"> <a href="#">More Info..</a></p>
                  </div>

                  <!--end iklan-->
                    
                </div>

            </div>
          </div>


        </div>
      </div>
      
      <br>
      <br>
      <br>

      @include('templates.footer')
@include('templates.foot')
</html>