<head>
  <title>{{$title}}</title>
  <link rel="shortcut icon" href="{{url('image/logo_wind.png')}}" />
  <!-- <link rel="stylesheet" type="text/css" href="./css/style.css"> -->
  <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
  
  <!-- Bootstrap Date-Picker Plugin -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>    
  <link rel="stylesheet" type="text/css" href="{{url('css/style2.css')}}">
</head>