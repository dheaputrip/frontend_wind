
<section class="ayam">

  <!---<div class="overlay"></div>-->
  
  <div class="inner">
      <div class="contentjumbo">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
          <a class="navbar-brand" href="{{route('home')}}"> <img src="{{url('image/logo_wind.png')}}" width="90" height="90" alt=""> </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="{{route('home')}}" style="color: #c7c7c7;">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('divisi')}}" style="color: #c7c7c7;"> Divisi</a>
              </li>
              <li class="nav-item dropdown active">
                      <a class="nav-link dropdown-toggle" style="color: #ffaa00;" href="berita.html" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Berita
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{route('match')}}">Match</a>
                          <a class="dropdown-item" href="{{route('berita')}}">Wind News</a>
                          <a class="dropdown-item" href="{{route('prestasi')}}">Prestasi</a>
                          <a class="dropdown-item" href="{{route('pendaftaran')}}">Pendaftaran</a>
                      </div>
                    </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('manajemen')}}" style="color: #c7c7c7;">Manajemen</a>
              </li>
              
            </ul>
          </div>
        </nav>
      </div>

  </div>
  </div>
  </div>
  </div>
  
  </section>