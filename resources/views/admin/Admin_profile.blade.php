<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

  <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
    <a class="navbar-brand" href="{{route('dashboard_admin')}}" style="color: #fff;"> <img src="{{url('image/logo_wind.png')}}"><span
        style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
    <button class="btn btn-warning" id="logout">Logout</button>
  </nav>
</div>

<div id="viewport">
  <!-- Sidebar -->
  @include('admin.template.sidebar')
  <!-- Content -->
  <div id="content">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container-fluid float-left pl-4">
      <!-- <TABEL UBAH JADWAL> -->
      <div class="">
        <div class="container">
          <div class="row justify-content-between">
            <div>
              <span style="color: red;"><b>Profile</b></span>
            </div>
            <div>
              <a href={{route('form_admin-change')}} class="btn btn-success btn-md">Ganti Password</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/admin_admin.js')}}">
</script>

</html>