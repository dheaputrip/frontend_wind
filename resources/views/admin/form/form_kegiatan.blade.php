<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
        <a class="navbar-brand" href="../../Admin.html" style="color: #fff;"> <img src="../../image/logo_wind.png"><span
                style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
        <button class="btn btn-warning" id="logout">Logout</button>
    </nav>
</div>

<div id="viewport">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- Content -->
    <div id="content">
        <nav class="navbar navbar-default">
            <!-- <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
          <li><a href="#">Kegiatan</a></li>
        </ul>
      </div> -->
        </nav>
        <div class="container-fluid float-left pl-4">
            <span style="color: red;"><b>Kegiatan</b></span>
            <hr>
            <form id="form" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="selectDivisi" class="col-sm-2 col-form-label">Pilih Divisi</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectDivisi" name="id_divisi">
                            <option value=1>E-sport</option>
                            <option value=2>Futsal U-13</option>
                            <option value=3>Futsal U-16</option>
                            <option value=4>Futsal U-19</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Nama Kegiatan</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputName" name="nama">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="selectJenis" class="col-sm-2 col-form-label">Jenis kegiatan</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectJenis" name="jenis">
                            <option value="kejuaraan">Kejuaraan</option>
                            <option value="latih_tanding">Latih Tanding</option>
                            <option value="latihan_rutin">Latihan Rutin</option>
                            <option value="kunjungan">Kunjungan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTempat" class="col-sm-2 col-form-label">Tempat</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputTempat" name="tempat">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKlub" class="col-sm-2 col-form-label">Nama Klub Lawan</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputKlub" name="klub_lawan">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSkorKandang" class="col-sm-2 col-form-label">Skor Kandang</label>
                    <div class="col-sm-auto">
                        <input type="number" min="0" value="0" class="form-control" id="inputSkorKandang" name="skor_kandang">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSkorTandang" class="col-sm-2 col-form-label">Skor Tandang</label>
                    <div class="col-sm-auto">
                        <input type="number" min="0" value="0" class="form-control" id="inputSkorTandang" name="skor_tandang">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectStatus" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectStatus" name="status">
                            <option value="belum_selesai">Belum Selesai</option>
                            <option value="selesai">Selesai</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggal" class="col-sm-2 col-form-label">Tanggal Mulai</label>
                    <div class="col-sm-auto">
                        <input type="date" class="form-control" id="inputTanggalMulai" name="tanggal_mulai">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJam" class="col-sm-2 col-form-label">Jam Mulai</label>
                    <div class="col-sm-auto">
                        <input type="time" class="form-control" id="inputJamMulai" name="jam_mulai">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggal" class="col-sm-2 col-form-label">Tanggal Selesai</label>
                    <div class="col-sm-auto">
                        <input type="date" class="form-control" id="inputTanggalSelesai" name="tanggal_selesai">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJamSelesai" class="col-sm-2 col-form-label">Jam Selesai</label>
                    <div class="col-sm-auto">
                        <input type="time" class="form-control" id="inputJamSelesai" name="jam_selesai">
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-warning">Upload</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/form/form_kegiatan.js')}}">
</script>

</html>