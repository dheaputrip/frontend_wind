<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
        <a class="navbar-brand" href="../../Admin.html" style="color: #fff;"> <img src="../../image/logo_wind.png"><span
                style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
        <button class="btn btn-warning" id="logout">Logout</button>
    </nav>
</div>

<div id="viewport">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- Content -->
    <div id="content">
        <nav class="navbar navbar-default">
            <!-- <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
          <li><a href="#">Kegiatan</a></li>
        </ul>
      </div> -->
        </nav>
        <div class="container-fluid float-left pl-4">
            <span style="color: red;"><b>Pelatih</b></span>
            <hr>
            <form method="post" enctype="multipart/form-data" id="form">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputName" name="nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectDivisi" class="col-sm-2 col-form-label">Pilih Divisi</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectDivisi" name="id_divisi">
                            <option value="2">Futsal U-13</option>
                            <option value="3">Futsal U-16</option>
                            <option value="4">Futsal U-19</option>
                            <option value="1">E-sport</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="inputAlamat" name="alamat" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTempatLahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputTempatLahir" name="tempat_lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalLahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-auto">
                        <input type="date" class="form-control" id="inputTanggalLahir" name="tanggal_lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputNIK" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputNIK" name="nik">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTinggiBadan" class="col-sm-2 col-form-label">Tinggi Badan</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputTinggiBadan" name="tinggi_badan">
                    </div>
                    <span>cm</span>
                </div>
                <div class="form-group row">
                    <label for="inputBeratBadan" class="col-sm-2 col-form-label">Berat Badan</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputBeratBadan" name="berat_badan">
                    </div>
                    <span>kg</span>
                </div>
                <div class="form-group row">
                    <label for="inputNomorHP" class="col-sm-2 col-form-label">Nomor HP</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputNomorHP" name="nomor_hp">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectJenisKelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectJenisKelamin" name="jenis_kelamin">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputFotoProfil" class="col-sm-2 col-form-label">Foto Profil</label>
                    <div class="col-sm-auto">
                        <div class="custom-file col-sm-auto">
                            <input type="file" class="custom-file-input" id="inputFotoProfil" name="foto_profil">
                            <label class="custom-file-label" for="inputFotoProfil">Pilih Foto</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-warning">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/form/form_pelatih.js')}}">
</script>

</html>