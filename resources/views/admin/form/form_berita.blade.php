<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
        <a class="navbar-brand" href="../../Admin.html" style="color: #fff;"> <img src="../../image/logo_wind.png"><span
                style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
        <button class="btn btn-warning" id="logout">Logout</button>
    </nav>
</div>

<div id="viewport">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- Content -->
    <div id="content">
        <nav class="navbar navbar-default">
            <!-- <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
          <li><a href="#">Kegiatan</a></li>
        </ul>
      </div> -->
        </nav>
        <div class="container-fluid float-left pl-4">
            <span style="color: red;"><b>Berita</b></span>
            <hr>
            <form id="formBerita">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Judul</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="inputName">
                    </div>
                </div>
                <form id="lampiranBerita">
                    <div class="form-group row">
                        <label for="inputFoto" class="col-sm-2 col-form-label">Gambar</label>
                        <div class="col-sm-4">
                            <div class="custom-file col-sm-auto">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Upload Image</label>
                            </div>
                        </div>
                        <div class="col-sm-2" style="position: relative;">
                            <button type="submit" class="btn btn-warning" id="uploadGambar">Upload Gambar</button>
                        </div>
                    </div>
                </form>
                <div class="form-group row">
                    <label for="inputTempat" class="col-sm-2 col-form-label">Isi Berita</label>
                    <div class="col-sm-6" id="text-tool">
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-warning">Upload</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/form/form_berita.js')}}">
</script>

</html>