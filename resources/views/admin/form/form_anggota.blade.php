<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
        <a class="navbar-brand" href="../../Admin.html" style="color: #fff;"> <img src="../../image/logo_wind.png"><span
                style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
        <button class="btn btn-warning" id="logout">Logout</button>
    </nav>
</div>

<div id="viewport">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- Content -->
    <div id="content">
        <nav class="navbar navbar-default">
            <!-- <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
          <li><a href="#">Kegiatan</a></li>
        </ul>
      </div> -->
        </nav>
        <div class="container-fluid float-left pl-4">
            <span style="color: red;"><b>Anggota</b></span>
            <hr>
            <form method="post" enctype="multipart/form-data" id="form">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputName" name="nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectDivisi" class="col-sm-2 col-form-label">Pilih Divisi</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectDivisi" name="id_divisi">
                            <option value=1>E-sport</option>
                            <option value=2>Futsal U-13</option>
                            <option value=3>Futsal U-16</option>
                            <option value=4>Futsal U-19</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputNoPunggung" class="col-sm-2 col-form-label">Nomor Punggung</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputNoPunggung" name="nomor_punggung">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTempatLahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputTempatLahir" name="tempat_lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalLahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-auto">
                        <input type="date" class="form-control" id="inputTanggalLahir" name="tanggal_lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectJenis" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectJenis" name="jenis_kelamin">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectDarah" class="col-sm-2 col-form-label">Golongan Darah</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectDarah" name="golongan_darah">
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="AB">AB</option>
                            <option value="O">O</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTinggi" class="col-sm-2 col-form-label">Tinggi Badan</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputTinggi" name="tinggi_badan">
                    </div>
                    <span>cm</span>
                </div>
                <div class="form-group row">
                    <label for="inputBerat" class="col-sm-2 col-form-label">Berat Badan</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputBerat" name="berat_badan">
                    </div>
                    <span>kg</span>
                </div>
                <div class="form-group row">
                    <label for="inputAgama" class="col-sm-2 col-form-label">Agama</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputAgama" name="agama">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAnak" class="col-sm-2 col-form-label">Anak ke-</label>
                    <div class="col-sm-auto">
                        <input type="number" class="form-control" id="inputAnak" name="anak_ke">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="inputAlamat" name="alamat" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputNomor" class="col-sm-2 col-form-label">Nomor HP</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputNomor" name="nomor_hp">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAsal" class="col-sm-2 col-form-label">Asal Sekolah</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputAsal" name="asal_sekolah">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputOrtu" class="col-sm-2 col-form-label">Nama Orangtua</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputOrtu" name="nama_ortu">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamatOrtu" class="col-sm-2 col-form-label">Alamat Orangtua</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="inputAlamatOrtu" name="alamat_ortu" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputFoto" class="col-sm-2 col-form-label">Foto</label>
                    <div class="col-sm-auto">
                        <div class="custom-file col-sm-auto">
                            <input type="file" class="custom-file-input" id="inputFoto" name="foto">
                            <label class="custom-file-label" for="customFile">Pilih File</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputHpOrtu" class="col-sm-2 col-form-label">Nomor HP Orangtua</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputHpOrtu" name="nomor_hp_ortu">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKerja" class="col-sm-2 col-form-label">Pekerjaan</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputKerja" name="pekerjaan">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectPenghasilan" class="col-sm-2 col-form-label">Penghasilan</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectPenghasilan" name="penghasilan">
                            <option value="< 500.000">< 500.000</option>
                            <option value="500.000 - 1.000.000">500.000 - 1.000.000</option>
                            <option value="1.000.001 - 2.000.000">1.000.001 - 2.000.000</option>
                            <option value="2.000.001 - 3.000.000">2.000.001 - 3.000.000</option>
                            <option value="3.000.001 - 4.000.000">3.000.001 - 4.000.000</option>
                            <option value="4.000.001 - 5.000.000">4.000.001 - 5.000.000</option>
                            <option value="> 5.000.0000"> > 5.000.0000 </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selectAnggota" class="col-sm-2 col-form-label">Status Keanggotaan</label>
                    <div class="col-sm-auto">
                        <select class="form-control" id="selectAnggota" name="status_anggota">
                            <option value="0">Calon Anggota</option>
                            <option value="1">Anggota Tetap</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-warning">Upload</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/form/form_anggota.js')}}">
</script>

</html>