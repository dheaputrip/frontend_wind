<!DOCTYPE html>
@include('admin.template.head')
<div class="inih">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-admin justify-content-between">
        <a class="navbar-brand" href="../../Admin.html" style="color: #fff;"> <img src="../../image/logo_wind.png"><span
                style="color: orange;">HAI,</span> <span id="nama_user"></span></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
        <button class="btn btn-warning" id="logout">Logout</button>
    </nav>
</div>

<div id="viewport">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- Content -->
    <div id="content">
        <nav class="navbar navbar-default">
            <!-- <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#"><i class="zmdi zmdi-notifications text-danger"></i>
            </a>
          </li>
          <li><a href="#">Kegiatan</a></li>
        </ul>
      </div> -->
        </nav>
        <div class="container-fluid float-left pl-4">
            <span style="color: red;"><b>Divisi</b></span>
            <hr>
            <form id="form" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Nama Divisi</label>
                    <div class="col-sm-auto">
                        <input type="text" class="form-control" id="inputNamaDivisi" name="nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJumlahAnggota" class="col-sm-2 col-form-label">Jumlah Anggota</label>
                    <div class="col-sm-auto">
                        <input type="number" min="0" class="form-control" id="inputJumlahAnggota" name="jumlah_anggota">
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-warning">Upload</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.template.foot')
<script src="{{url('js/admin/form/form_divisi.js')}}">
</script>

</html>