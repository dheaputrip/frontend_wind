<head>
<title>Halaman Admin | {{ $title }}</title>
    <link rel="shortcut icon" href="{{url('image/logo_wind.png')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/Bootstrap-4-4.1.1/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/DataTables-1.10.20/css/jquery.dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/Responsive-2.2.3/css/responsive.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('froala_editor_3.0.6/css/froala_editor.pkgd.min.css')}}"/>
    {{-- <link rel="stylesheet" type="text/css" href="{{url('DataTables/RowGroup-1.1.1/css/rowGroup.bootstrap4.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{url('DataTables/AutoFill-2.3.4/css/autoFill.bootstrap4.css')}}"> --}}
    
    {{-- <link rel="stylesheet" type="text/css" href="{{url('css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/dataTables.bootstrap4.min.css')}}"> --}}
    <!-- <link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
    <link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.20/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.20/css/jquery.dataTables.css"> -->
  
  </head>