<script src="{{url('js/jquery-3.4.1.min.js')}}"></script>
<script src="{{url('js/popper.min.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/js.cookie.min.js')}}"></script>
<script src="{{url('js/axios.min.js')}}"></script>
<script src="{{url('js/sweetalert2.all.min.js')}}"></script>
<script src="{{url('js/pretty.js')}}"></script>
<script src="{{url('DataTables/Bootstrap-4-4.1.1/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('froala_editor_3.0.6/js/froala_editor.pkgd.min.js')}}"></script>

{{-- <script src="{{url('DataTables/datatables.js')}}"></script> --}}
{{-- <script src="{{url('DataTables/DataTables-1.10.20/js/jquery.dataTables.js')}}"></script> --}}
{{-- <script src="{{url('DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.js')}}"></script> --}}
{{-- <script src="{{url('DataTables/Responsive-2.2.3/js/responsive.bootstrap4.js')}}"></script> --}}
{{-- <script src="{{url('DataTables/RowGroup-1.1.1/js/rowGroup.bootstrap4.js')}}"></script> --}}
{{-- <script src="{{url('DataTables/AutoFill-2.3.4/js/autoFill.bootstrap4.js')}}"></script> --}}
<script src="{{url('js/config.js')}}"></script>
<script src="{{url('js/main.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
{{-- <script src="{{url('js/jquery.dataTables.min.js')}}"></script> --}}
{{-- <script src="{{url('js/dataTables.bootstrap4.min.js')}}"></script> --}}
<!-- <script src="DataTables/datatables.js"></script>
<script src="DataTables/DataTables-1.10.20/js/jquery.dataTables.js"></script>
<script src="DataTables/DataTables-1.10.20/js/dataTables.bootstrap.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->
<!-- <script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script> -->