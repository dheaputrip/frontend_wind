<div id="sidebar">
    <!-- <header>
      <a href="#">WIND JUNIOR SPORT COMMUNITY</a>
    </header> -->
    <ul class="nav">
      <li id="dashboard" class="active d-none">
        <a href={{route('dashboard_admin')}}>
          <i class="zmdi zmdi-view-dashboard"></i> Administrasi
        </a>
      </li>
      <li id="profile" class="d-none">
        <a href={{route('profile_admin')}}>
          <i class="zmdi zmdi-view-dashboard"></i> Profil
        </a>
      </li>
      <li>
        <a href= {{route('admin_kegiatan')}}>
          <i class="zmdi zmdi-link"></i> Kegiatan
        </a>
      </li>
      {{-- <li>
        <a href={{route('')}}>
          <i class="zmdi zmdi-widgets"></i> Jadwal
        </a>
      </li> --}}
      <li>
        <a href={{route('admin_divisi')}}>
          <i class="zmdi zmdi-calendar"></i> Divisi
        </a>
      </li>
      <li>
        <a href={{route('admin_anggota')}}>
          <i class="zmdi zmdi-info-outline"></i> Anggota
        </a>
      </li>
      <li>
        <a href={{route('admin_pelatih')}}>
          <i class="zmdi zmdi-settings"></i> Pelatih
        </a>
      </li>
      <li>
        <a href="#">
          <i class="zmdi zmdi-comment-more"></i> Manajerial
        </a>
      </li>
      <li>
        <a href="#">
          <i class="zmdi zmdi-comment-more"></i> Statistik
        </a>
      </li>
      <li>
        <a href={{route('admin_berita')}}>
          <i class="zmdi zmdi-comment-more"></i> Berita
        </a>
      </li>
    </ul>
  </div>