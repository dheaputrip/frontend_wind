<!DOCTYPE html>
@include('templates.head')
@include('templates.header')

<div class="match">
    <div class="dropdown-match">
        <div class="dropdown">
            <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Waktu
            </a>
          
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="#">Akan Berlangsung</a>
              <a class="dropdown-item" href="#">Selesai</a>
          
            </div>
          </div>
    </div>
  <h1> Match Futsal U13</h1>
  
  <hr>
  <div class="skor">
    <div class="container">
      <h2>Tanggal</h2>
      <h2>Lokasi</h2>
      <div class="row" style="text-align: center;">
        <div class="col nama-tim">
          TIM A
          <p class="hasil"> 5</p>
        </div>
        <div class="col col-vs">VS</div>
        <div class="col nama-tim">
              Tim B
          <p class="hasil"> 5</p>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
<br>
<br>

<div class="match">
    <div class="dropdown-match">
        <div class="dropdown">
            <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Waktu
            </a>
          
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="#">Akan Berlangsung</a>
              <a class="dropdown-item" href="#">Selesai</a>
          
            </div>
          </div>
    </div>
  <h1> Match Futsal U16</h1>
  
  <hr>
  <div class="skor">
    <div class="container">
      <h2>Tanggal</h2>
      <h2>Lokasi</h2>
      <div class="row" style="text-align: center;">
        <div class="col nama-tim">
          TIM A
          <p class="hasil"> 5</p>
        </div>
        <div class="col col-vs">VS</div>
        <div class="col nama-tim">
              Tim B
          <p class="hasil"> 5</p>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
<br>
<br>

<div class="match">
    <div class="dropdown-match">
        <div class="dropdown">
            <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Waktu
            </a>
          
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="#">Akan Berlangsung</a>
              <a class="dropdown-item" href="#">Selesai</a>
            </div>
          </div>
    </div>

  <h1> Match Futsal U19</h1>
  <hr>
  <div class="skor">
    <div class="container">
      <h2>Tanggal</h2>
      <h2>Lokasi</h2>
      <div class="row" style="text-align: center;">
        <div class="col nama-tim">
          TIM A
          <p class="hasil"> 5</p>
        </div>
        <div class="col col-vs">VS</div>
        <div class="col nama-tim">
              Tim B
          <p class="hasil"> 5</p>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
<br>
<br>




<div class="match">
  <div class="container">
    <div class="row justify-content-between">
      <div class="col">
        <h1> Match E-Sport</h1>
      </div>
      <div class="col align-self-center">
        <select id="status-esport" class="custom-select custom-select-lg float-right">
            <option selected value='belum_selesai'>Akan berlangsung</option>
            <option value='selesai'>Selesai</option>
        </select>
      </div>
    </div>
  </div>
  <hr>
  <div class="skor">
    <div class="container">
      <h2 id= "tanggal-mulai"></h2>
      <h2 id= "lokasi"></h2>
      <div class="row" style="text-align: center;">
        <div class="col nama-tim">
          WIND JUNIOR
          <p class="hasil" id="skor-kandang"></p>
        </div>
        <div class="col col-vs">VS</div>
        <div class="col nama-tim" >
          <span id="tim-lawan"></span>
          <p class="hasil" id="skor-lawan"></p>
        </div>
      </div>
    </div>
  </div>
</div>


@include('templates.footer')
@include('templates.foot')
<script src="{{url('js/user/match.js')}}"></script>
</html>
