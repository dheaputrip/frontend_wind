<!DOCTYPE html>
@include('templates.head')
@include('templates.header')



<div class="row no-gutters"> 
    <div class="col no-gutters">
        <div class="leftside">
            <div class="overlay"> <a href="{{route('divisi')}}"> <h1> Futsal</h1></a>
            </h1>
                </div>
        </div>
    </div>

    <div class="col no-gutters">
        <div class="rightside">
            <div class="overlay"> 
                <a href="{{route('divisi')}}"> <h1> Esport</h1></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cerita-wind">
        <div class="text-wind">
            <h1> Cerita Singkat Wind Junior</h1>
                Wind junior merupakan sebuah komunitas futsal yang berdomisili di Kampung bandan, Jakarta utara. Fokus Dari wind junior sendiri selain menghasilkan atlet yang handal dibidang futsal, juga bertujuan untuk mewadahi setiap potensi serta memberikan sebuah pendidikan karakter melalui olahraga bagi remaja kampung bandan.

        </div>
</div>


<div class="video-wind">
        <div class="row no-gutters"> 
                <div class="col no-gutters">
                    <div class="video">
                        <div class="tempat-video">
                                <div class="embed-responsive embed-responsive-4by3">
                                    <iframe width="480" height="215" src="https://www.youtube.com/embed/65kb4vzX9dc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                        </div>
                    </div>
                </div>
            
                <div class="col no-gutters">
                    <div class="informasi">
                         <h1> COMPANY PROFILE WIND JUNIOR</h1>
                        </div>
                    </div>
                </div>
            </div>
                      
</div>

@include('templates.footer')
@include('templates.foot')
</html>