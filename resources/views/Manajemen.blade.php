<!DOCTYPE html>
@include('templates.head')
@include('templates.header')
          
 
<div class="visi-misi">
        <div class="text-visi">
            <h1> Visi</h1>
            Mewujudkan Generasi Yang Mandiri, Tangguh, Terampil, Berakhlak, dan Berkualitas. Serta  Meningkatkan Partisipasi Pemuda Dalam Kegiatan-Kegiatan dan Pembinaan olahraga maupun kegiatan kepemudaan lainnya di Masyarakat Guna Memaksimalkan Sumber Daya Manusia”.
       <br>
       <br>
       <h1> Misi</h1>
            1.	Mengembangkan potensi pemuda yang ada di dalam masyarakat. <br>
            2.	Mengadakan kegiatan-kegiatan kepemudaan dalam masyarakat. <br>
            3.	Melibatkan pemuda dalam kegiatan-kegiatan organisasi maupun kegiatan aktif lainnya. <br>
            4.	Mengembangkan kreativitas dan bakat pemuda melalui pendidikan dan pelatihan di bidang olah raga maupun kepemudaan. <br>
            5.	Turut serta membantu dalam pengabdian masyarakat. <br>
            6.	Melestarikan nilai-nilai sosial, seni dan budaya masyarakat Indonesia . <br>
          </div>
</div>

<!--  
<div class="prestasi">
  <div class="text-prestasi">
      <h1> Prestasi Wind junior</h1>
     1. Juara 1 FORST Futsal Ancol 2019 (U13) <br>
     2. Juara 1 FORST Futsal Pademangan 2019 (U13) <br>
     3. Juara 1 Lodan Cup 2019 (U13) <br>
     4. Juara 1 BHK Tournament 2019 (U16) <br>
     3. Juara 1 Beye Fams x Nord Gaming Tournament (Esports) <br>
     4. Juara 2 Tournament Mobile Legends MyBaper S1 <br>
    </div>
</div> -->

<div class="kepengurusan">
  <div class="box-kepengurusan">
    
<div class="manajemen">
    <h1> KEPENGURUSAN</h1>
  <div class="container-team">
    <div class="row info-team">
      <div class="col-sm-6 nama-team">
        <div class="position">
            <button type="button" class="btn btn-warning btn-posisi">K</button>
          <p class="nama-pemain"> Ketua </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> SekBen </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Head Coach Futsal </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Futsal U13 </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Futsal U16 </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Futsal U19 </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain">Divisi Esports 1 </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Esports 2 </p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Humas 1</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-warning btn-posisi">K</button>
          <p class="nama-pemain"> Divisi humas 2</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-warning btn-posisi">K</button>
          <p class="nama-pemain"> Divisi humas 3</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Perlengkapan 1</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Perlengkapan 2</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Sumber daya 1</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Sumber daya 2</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Sumber daya 3</p>
        </div>
        <div class="position">
            <button type="button" class="btn btn-primary btn-posisi">P</button>
          <p class="nama-pemain"> Divisi Sumber daya 4</p>
        </div>




      </div>
  
  
      <div class="col-sm-6 foto">
          <div class="foto"><img src="image/bayu.jpg"></div>
          <div class="informasi">
          <div class="container-informasi">
              <div class="row">
                  <div class="col-sm-4">Nama </div>
                  <div class="com-sm-2"> : </div>
                <div class="col-sm-7">Nugroho Saputra</div>
  
                <div class="col-sm-4">Umur </div>
                <div class="com-sm-2"> : </div>
              <div class="col-sm-7">30 Tahun</div>

              <div class="col-sm-4">Tinggi Badan </div>
                <div class="com-sm-2"> : </div>
              <div class="col-sm-7">175cm</div>

              <div class="col-sm-4">Berat Badan </div>
                <div class="com-sm-2"> : </div>
              <div class="col-sm-7">67kg</div>
               </div>
          </div>
          </div>
          
        </div>
    </div>
  
  
  </div>
  
  
  </div>
  </div>
</div>

@include('templates.footer')
@include('templates.foot')
</html>
