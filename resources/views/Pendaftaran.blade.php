<!DOCTYPE html>
@include('templates.head')
@include('templates.header')


<div class="pendaftaran">
    <h1> Persyaratan Pendaftaran</h1>
    <div class="syarat-pendaftaran">

        1. Berusia minimal 5 ( lima ) tahun dan maksimal 18 tahun (delapan belas) tahun,sehat Jasmani dan Rohani. <br>
        2. Mengambil formulir  pendaftaran keanggotaan siswa Wind Junior <br>
        3. Mengisi formulir pendaftaran menjadi anggota / siswa Wind Junior Mengisi formulir Surat Pernyataan
        Persetujuan orang tua/wali siswa menjadi anggota/siswa Wind Junior , dengan dibubuhi tanda tangan orang tua /
        siswa. <br>
        4. Melampirkan masing-masing 1 (satu) lembar fotocopy : <br>
        - Ijasah/STTB ( Surat Tanda Tamat Belajar ) <br>
        - Raport bagian depan <br>
        - Kartu Keluarga <br>
        - Akte Kelahiran <br>
        - KTP orang tua /wali siswa <br>
        - Melampirkan pas photo berwarna terbaru ukuran  : 2 x 3 sebanyak 4 (empat) lembar <br>

        <br>
        <br>

    </div>
</div>

<div class="tutup">
    <h1> !!! PENDAFTARAN MUSIM INI BELUM DIBUKA !!! </h1>

</div>
<div class="formulir-pendaftaran">
    <h1> Formulir pendaftaran</h1>
    <h2> A.Data diri</h2>
    <div class="row formulir">
        <div class="col-sm-3">
            <p class="info-formulir"> Nama </p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control" id="nama" name="nama" aria-describedby="emailHelp"
                        placeholder="Masukan nama anda">
                    <small id="emailHelp" class="form-text text-muted">Masukan Nama Anda</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <!--Nomor Punggung-->
        <div class="col-sm-3">
            <p class="info-formulir"> Nomor Punggung </p>
        </div>
        <div class="col-sm-9">
            <div class="col-xs-2">
                <form>
                    <div class="form-group">
                        <input type="text" name="nomor_punggung" id="nomor_punggung" size="5" maxlength="2">
                        <small id="emailHelp" class="form-text text-muted">Masukan nomor punggung</small>
                        <label for="nama"></label>
                    </div>
                </form>

            </div>
        </div>

        <!--TTL-->
        <div class="col-sm-3">
            <p class="info-formulir"> Tempat & Tanggal Lahir</p>
        </div>
        <div class="col-sm-9">
            <form method="post">
                <div class="form-group">
                    <!-- Date input -->
                    <input class="form-control" id="tanggal_lahir" name="tanggal__lahir" type="date" />
                    <label></label>
                </div>
            </form>
        </div>

        <!--Jenis Kelamin-->
        <div class="col-sm-3">
            <p class="info-formulir"> Jenis Kelamin</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <select class="custom-select my-1 mr-sm-3" id="jenis_kelamin" name="jenis_kelamin">
                        <option selected>Pilih</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                    <label class="my-1 mr-2" for="inlineFormCustomSelectPref"></label>
                </div>
            </form>
        </div>


        <div class="col-sm-3">
            <p class="info-formulir"> Golongan Darah</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <select class="custom-select my-1 mr-sm-3" id="golongan_darah" name="golongan_darah">
                        <option selected>Pilih</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
                        <option value="0">Tidak Tahu</option>
                    </select>
                    <label class="my-1 mr-2" for="inlineFormCustomSelectPref"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> Tinggi Badan</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <input type="text" name="email" size="5" maxlength="3" id="tinggi_badan" name="tinggi_badan"> cm
                    <small id="emailHelp" class="form-text text-muted">Masukan Nama Anda</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> Berat Badan </p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <input type="text" name="berat" size="5" maxlength="3" id="berat_badan"> Kg
                    <small id="emailHelp" class="form-text text-muted">Masukan berat badan</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir">Anak Ke</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <select class="custom-select my-1 mr-sm-3" id="anak_ke">
                        <option selected>Pilih</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="2">3</option>
                        <option value="2">4</option>
                        <option value="2">5</option>
                        <option value="2">6</option>
                        <option value="2">7</option>
                        <option value="2">8</option>
                        <option value="2">9</option>
                        <option value="2">10</option>
                    </select>
                    <label class="my-1 mr-2" for="inlineFormCustomSelectPref"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> Alamat</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <textarea class="form-control" rows="5" id="alamat"></textarea>
                    <small id="Alamat" class="form-text text-muted">Masukan alamat anda</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> No. Telepon </p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">

                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Masukan nomor telepon anda">
                    <small id="emailHelp" class="form-text text-muted">Masukan nomor telepon anda</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>


        <div class="col-sm-3">
            <p class="info-formulir"> Asal Sekolah </p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control" id="asal_sekolah" aria-describedby="School"
                        placeholder="Masukan sekolah anda">
                    <small id="emailHelp" class="form-text text-muted">Masukan sekolah anda</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>
    </div>


    <h2> A.Data Wali</h2>
    <div class="row formulir">
        <div class="col-sm-3">
            <p class="info-formulir"> Nama Orangtua / Wali</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">

                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Masukan nama orangtua / wali">
                    <small id="emailHelp" class="form-text text-muted">Masukan nama orangtua / wali </small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir">Pekerjaan </p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">

                    <input type="text" class="form-control" id="pekerjaan_orangtua" aria-describedby="Occupation"
                        placeholder="Pekerjaan orang tua">
                    <small id="emailHelp" class="form-text text-muted">Masukan pekerjaan orang tua</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> Alamat</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">

                    <textarea class="form-control" rows="5" id="alamat_orangtua"></textarea>
                    <small id="Alamat" class="form-text text-muted">Masukan alamat orang tua</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> No.Telp</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">

                    <input type="number" class="form-control" id="nomor_telp" aria-describedby="emailHelp"
                        placeholder="Masukan Nomor Telepon">
                    <small id="alamat" class="form-text text-muted">Masukan nomor telepon orang tua</small>
                    <label for="nama"></label>
                </div>
            </form>
        </div>

        <div class="col-sm-3">
            <p class="info-formulir"> Penghasilan Orang Tua</p>
        </div>
        <div class="col-sm-9">
            <form>
                <div class="form-group">
                    <select class="custom-select my-1 mr-sm-3" id="penghasilan">
                        <option selected>Pilih</option>
                        <option value="1">
                            < 500.000</option> <option value="2">500.000 - 1.000.000
                        </option>
                        <option value="2">1.000.001 - 2.000.000</option>
                        <option value="2">2.000.001 - 3.000.000</option>
                        <option value="2">3.000.001 - 4.000.000</option>
                        <option value="2">4.000.001 - 5.000.000</option>
                        <option value="2"> > 5.000.0000 </option>
                    </select>
                    <label class="my-1 mr-2" for="inlineFormCustomSelectPref"></label>
                </div>
                <div class="form-group row mr-5 invisible">
                    <div class="col-sm-1">
                        <input type="number" min="0" max="1" class="btn btn-warning" id="status_anggota"
                            name="status_anggota" value="0">
                    </div>
                </div>
                <div class="form-group row mr-5">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-daftar">Daftar</button>
                    </div>
                </div>
            </form>
        </div>


    </div>

</div>

@include('templates.footer')
@include('templates.foot')

</html>
