<!DOCTYPE html>
@include('templates.head')
@include('templates.header')
<div class="container-fluid detail-berita px-5">
    {{-- <div class="row justify-content-between">
        <div class="col align-self-center pl-5">
            <h1> SQUAD <span id="nama-team"></span></h1>
        </div>
        <div class="col align-self-center pr-5">
            <div class="w-50 float-right">
                <select class="custom-select custom-select-lg" id="pilih-team">
                        <option selected value=1>Esport</option>
                        <option value=2>Fusal U13</option>
                        <option value=3>Fusal U16</option>
                        <option value=4>Fusal U19</option>
                </select>
            </div>
        </div>
    </div> --}}
    {{-- <div class="row justify-content-start mb-3">
        <div class="col-4 pl-5">
            <div class="pilih-pemanin">
                <select class="custom-select custom-select-lg" id="pilih-pemain">
                    <option selected value=0>Pilih Pemain</option>
                </select>
            </div>
        </div>
    </div> --}}
    <nav class="pb-5">
        <div class="row nav nav-tabs text-center" id="nav-tab" role="tablist">
            <a class="col nav-item nav-link active" id="nav-e-sport-tab" data-toggle="tab" data-id=1 href="#nav-e-sport" role="tab" aria-controls="nav-e-sport" aria-selected="true"><h1>E-sport</h1></a>
            <a class="col nav-item nav-link" id="nav-u-13-tab" data-toggle="tab" data-id=2 href="#nav-u-13" role="tab" aria-controls="nav-u-13" aria-selected="false"><h1>Futsal U-13</h1></a>
            <a class="col nav-item nav-link" id="nav-u-16-tab" data-toggle="tab" data-id=3 href="#nav-u-16" role="tab" aria-controls="nav-u-16" aria-selected="false"><h1>Futsal U-16</h1></a>
            <a class="col nav-item nav-link" id="nav-u-19-tab" data-toggle="tab" data-id=4 href="#nav-u-19" role="tab" aria-controls="nav-u-19" aria-selected="false"><h1>Futsal U-19</h1></a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        {{-- Tab E-Sport --}}
        <div class="tab-pane fade show active" id="nav-e-sport" role="tabpanel" aria-labelledby="nav-e-sport-tab">
            <div class="row">
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pemain">
                            {{-- <option selected value=0>Pilih Pemain</option> --}}
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pemain</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td class="nama-pemain"></th>
                                    </tr>
                                    <tr class="nomor-punggung">
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td class="umur-pemain"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td class="jk-pemain"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td class="tinggi-pemain"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td class="berat-pemain"></td>
                                    </tr>
                                    <tr>
                                        <th class="divisi-pemain" scope="row"></th>
                                        <td>:</td>
                                        <td class="posisi-pemain"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pelatih">
                            <option selected value=0>Pilih Pelatih</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pelatih</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-official">
                            <option selected value=0>Pilih Official</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Official</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Tab Futsal U-13 --}}
        <div class="tab-pane fade" id="nav-u-13" role="tabpanel" aria-labelledby="nav-u-13-tab">
            <div class="row">
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pemain">
                            <option selected value=0>Pilih Pemain</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pemain</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pelatih">
                            <option selected value=0>Pilih Pelatih</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pelatih</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-official">
                            <option selected value=0>Pilih Official</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Official</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Tab Futsal U-16 --}}
        <div class="tab-pane fade" id="nav-u-16" role="tabpanel" aria-labelledby="nav-u-16-tab">
                <div class="row">
                    <div class="col-lg col-sm-12">
                        <div class="card mb-3">
                            <select class="custom-select custom-select-lg pilih-pemain">
                                <option selected value=0>Pilih Pemain</option>
                            </select>
                            <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h4 class="card-title">Profile Pemain</h4>
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr scope="row">
                                            <th>Nama</th>
                                            <td>:</th>
                                            <td>Bayu Anggara</th>
                                        </tr>
                                        <tr>
                                            <th scope="row">Nomor Punggung</th>
                                            <td>:</td>
                                            <td>16</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Umur</th>
                                            <td>:</td>
                                            <td>15 Tahun</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Jenis kelamin</th>
                                            <td>:</td>
                                            <td>Laki-laki</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Tinggi Badan</th>
                                            <td>:</td>
                                            <td>156 cm</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Berat Badan</th>
                                            <td>:</td>
                                            <td>56 kg</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Posisi</th>
                                            <td>:</td>
                                            <td>Kiper</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg col-sm-12">
                        <div class="card mb-3">
                            <select class="custom-select custom-select-lg pilih-pelatih">
                                <option selected value=0>Pilih Pelatih</option>
                            </select>
                            <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h4 class="card-title">Profile Pelatih</h4>
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr scope="row">
                                            <th>Nama</th>
                                            <td>:</th>
                                            <td>Bayu Anggara</th>
                                        </tr>
                                        <tr>
                                            <th scope="row">Nomor Punggung</th>
                                            <td>:</td>
                                            <td>16</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Umur</th>
                                            <td>:</td>
                                            <td>15 Tahun</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Jenis kelamin</th>
                                            <td>:</td>
                                            <td>Laki-laki</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Tinggi Badan</th>
                                            <td>:</td>
                                            <td>156 cm</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Berat Badan</th>
                                            <td>:</td>
                                            <td>56 kg</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Posisi</th>
                                            <td>:</td>
                                            <td>Kiper</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg col-sm-12">
                        <div class="card mb-3">
                            <select class="custom-select custom-select-lg pilih-official">
                                <option selected value=0>Pilih Official</option>
                            </select>
                            <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h4 class="card-title">Profile Official</h4>
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr scope="row">
                                            <th>Nama</th>
                                            <td>:</th>
                                            <td>Bayu Anggara</th>
                                        </tr>
                                        <tr>
                                            <th scope="row">Nomor Punggung</th>
                                            <td>:</td>
                                            <td>16</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Umur</th>
                                            <td>:</td>
                                            <td>15 Tahun</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Jenis kelamin</th>
                                            <td>:</td>
                                            <td>Laki-laki</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Tinggi Badan</th>
                                            <td>:</td>
                                            <td>156 cm</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Berat Badan</th>
                                            <td>:</td>
                                            <td>56 kg</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Posisi</th>
                                            <td>:</td>
                                            <td>Kiper</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        {{-- Tab Futsal U-19 --}}
        <div class="tab-pane fade" id="nav-u-19" role="tabpanel" aria-labelledby="nav-u-19-tab">
            <div class="row">
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pemain">
                            <option selected value=0>Pilih Pemain</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pemain</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-pelatih">
                            <option selected value=0>Pilih Pelatih</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Pelatih</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-sm-12">
                    <div class="card mb-3">
                        <select class="custom-select custom-select-lg pilih-official">
                            <option selected value=0>Pilih Official</option>
                        </select>
                        <img src="{{url('image/bayu.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">Profile Official</h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr scope="row">
                                        <th>Nama</th>
                                        <td>:</th>
                                        <td>Bayu Anggara</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Punggung</th>
                                        <td>:</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Umur</th>
                                        <td>:</td>
                                        <td>15 Tahun</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelamin</th>
                                        <td>:</td>
                                        <td>Laki-laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tinggi Badan</th>
                                        <td>:</td>
                                        <td>156 cm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Badan</th>
                                        <td>:</td>
                                        <td>56 kg</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posisi</th>
                                        <td>:</td>
                                        <td>Kiper</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    
</div>

{{-- 
<div class="detail-berita">
    <div class="dropdown-team">
        <select id="pilih-team">
            <option selected value=1>Esport</option>
            <option value=2>Fusal U13</option>
            <option value=3>Fusal U16</option>
            <option value=4>Fusal U19</option>
        </select>
    </div>
    <h1> SQUAD <span id="nama-team"></span></h1>

    <hr>

    <div class="pilih-pemanin ml-5">
        <select id="pilih-pemain">
            <option selected value=0>Pilih Pemain</option>
        </select>
    </div>
</div>
<br>
<br>
<div class="container-team">
    <div class="row info-team">
        <div class="col-sm-6 nama-team">
            <h3>Profil Pemain</h3>
            <div class="row">
                <div class="col-6 info-pemain">
                    <ul style="list-style-type:none">
                        <li class="informasi-pemain"> Nama
                        <li class="informasi-pemain"> Nomor Punggung
                        <li class="informasi-pemain"> Umur
                        <li class="informasi-pemain"> Jenis Kelamin
                        <li class="informasi-pemain"> Tinggi Badan
                        <li class="informasi-pemain"> Berat-badan
                        <li class="informasi-pemain"> Posisi
                    </ul>
                </div>
                <div class="col-6 nama-team">
                    <div class="detail-informasi">
                        <p>
                            : Bayu Anggara <br>
                            : 16 tahun <br>
                            : 15 Tahun<br>
                            : Laki-Laki<br>
                            : 156 cm <br>
                            : 68kg<br>
                            : <br>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-6 foto">
            <div class="foto"><img src="image/bayu.jpg"></div>
            <div class="informasi">
                <div class="container-informasi">
                    <div class="row">
                        <div class="col-sm-4">Nama </div>
                        <div class="com-sm-2"> : </div>
                        <div class="col-sm-7">4</div>

                        <div class="col-sm-4">Umur </div>
                        <div class="com-sm-2"> : </div>
                        <div class="col-sm-7">18 Tahun</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<!--Pilih Pelatih-->

<div class="pilih-pemanin">
    <div class="dropdown-pemain">
        <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            Pilih Pelatih
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="#">Bayu Anggara</a>
        </div>
    </div>
</div>
<br>
<br>
<div class="container-team">
    <div class="row info-team mb-5">
        <div class="col-sm-6 nama-team">
            <h3>Profil Pemain</h3>
            <div class="row">
                <div class="col-6 info-pemain">
                    <ul style="list-style-type:none">
                        <li class="informasi-pemain"> Nama
                        <li class="informasi-pemain"> Nomor Punggung
                        <li class="informasi-pemain"> Umur
                        <li class="informasi-pemain"> Jenis Kelamin
                        <li class="informasi-pemain"> Tinggi Badan
                        <li class="informasi-pemain"> Berat-badan
                        <li class="informasi-pemain"> Posisi
                    </ul>
                </div>
                <div class="col-6 nama-team">
                    <div class="detail-informasi">
                        <p>
                            : Bayu Anggara <br>
                            : 16 tahun <br>
                            : 15 Tahun<br>
                            : Laki-Laki<br>
                            : 156 cm <br>
                            : 68kg<br>
                            : <br>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 foto">
            <div class="foto"><img src="image/bayu.jpg"></div>
            <div class="informasi">
                <div class="container-informasi">
                    <div class="row">
                        <div class="col-sm-4">Nama </div>
                        <div class="com-sm-2"> : </div>
                        <div class="col-sm-7">4</div>
                        <div class="col-sm-4">Umur </div>
                        <div class="com-sm-2"> : </div>
                        <div class="col-sm-7">18 Tahun</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

@include('templates.footer')
@include('templates.foot')
<script src="{{url('js/user/team.js')}}"></script>

</html>
