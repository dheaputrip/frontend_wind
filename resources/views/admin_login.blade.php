<!DOCTYPE html>
@include('admin.template.head')

<!-- <head>
    <title> Login Admin</title>
    <link rel="shortcut icon" href="image/logo_wind.png" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head> -->
<section class="intro">

    <!---<div class="overlay"></div>-->

    <!-- <div class="inner">
                <div class="content">
                    <h1>Login</h1>
                    <div class="row">
                        <div class="col-4">
                            <img class="logo-wind" src="image/logo_wind.png">
                        </div>
                        <div class="col-7">
                            <h1> Login</h1>
                            <div class="informasi"> Sport Community</div>
                            <a class="btn" href=""> Let's Kick In</a>
                            <button class="button" href="home.html"> <a href="home.html" style="color: #b20000;"> Let's Kick in!</a> </button>
                        </div>
                    </div>
        
                </div>
        
            </div> -->
    <div class="login">
        <div class="isi-login">
            <form id="formLogin" method="POST">
                <h1> login</h1>
                <h2> Username</h2>
                <div class="row">
                    <div class="col-sm-5 col-login">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" id="username"
                                aria-describedby="emailHelp" placeholder="Masukan username">
                        </div>
                    </div>
                </div>

                <h2> Password</h2>
                <div class="row">
                    <div class="col-sm-5 col-login">
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="password"
                                aria-describedby="emailHelp" placeholder="Password Anda">
                        </div>
                    </div>
                </div>
                <button class="button-login" type="submit"> Masuk </button>
            </form>
        </div>
    </div>
    </div>
    </div>
    </div>

</section>
{{-- @include('admin.template.foot') --}}
<script type="text/javascript" crossorigin
    src="https://unpkg.com/universal-cookie@3/umd/universalCookie.min.js"></script>
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.js"></script>

<script type="text/javascript" src="js/js.cookie.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>
<script
    src="https://raw.githubusercontent.com/ManivannanMurugavel/QuestionandAnswer/master/static/js/js.cookie.js"></script>
<script type="text/javascript" src="js/mdb.js"></script>
<script type="text/javascript" src="js/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="js/config.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

<script>
    if (Cookies.get('nubmaster69')) {
        window.location.href = vBaseURL + "/admin/dashboard";
    }

    $('#formLogin').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serialize();
        console.log(data);
        $.ajax({
            method: 'POST',
            url: baseURL + '/api/auth/login',
            data
        }).done(function (response) {
                // console.log(response.token);
                // Cookies.set('nubmaster69', response.token, {
                //     expires: 1,
                //     path: '/'
                // });
                // Swal.fire({
                //     type: 'success',
                //     title: 'Berhasil Login!',
                //     text: 'Something went wrong!',
                //     timer: 1500,
                //     footer: '<a href>Why do I have this issue?</a>'
                // });
                if (response.user.role == 'super_admin') {
                    // console.log(response.token);
                    Cookies.set('nubmaster69', response.token, {
                        expires: 1,
                        path: '/'
                    });
                    Swal.fire({
                        type: 'success',
                        title: 'Berhasil Login!',
                        //text: 'Something went wrong!',
                        timer: 1500,
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                    setTimeout(function () {
                        window.location.href = vBaseURL + '/admin/dashboard';
                    }, 1200);
                } else {
                    // console.log(response.token);
                    Cookies.set('nubmaster69', response.token, {
                        expires: 1,
                        path: '/'
                    });
                    Swal.fire({
                        type: 'success',
                        title: 'Berhasil Login!',
                        //text: 'Something went wrong!',
                        timer: 1500,
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                    setTimeout(function () {
                        // window.location.href = 'http://localhost/tampilan-laravel/public/admin/dashboard';
                        window.location.href = vBaseURL + '/admin/profile';
                    }, 1200);
                }
        }).fail(function (error) {
            // console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: error.responseJSON.message,
                //footer: '<a href>Why do I have this issue?</a>'
            })
        })
    });
    /*
    $('#formLogin').submit(async function (e) {
        e.preventDefault();
        let form = $(this);
        let data = form.serialize();
        console.log(data);
        try {
            const res = await fetch('http://127.0.0.1:8000/api/auth/login', {
                method: 'post',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                    'Accept': 'application/json',
                },
                body: data,
                mode: 'no-cors'
            })

            // const response = JSON.parse(res.text());
            console.log(res);
            const response = res.json();
            Cookies.set('nubmaster69', response.access_token, { expires: 1, path: '/' });
            // console(Cookies.get('nubmaster69'));
            // if (response.role === 'admin') {
            //alert(response.message);
            Swal.fire({
                type: 'success',
                title: 'Berhasil Login!',
                //text: 'Something went wrong!',
                timer: 1500,
                //footer: '<a href>Why do I have this issue?</a>'
            });
            // setTimeout(function () {
            //     window.location.replace('Admin_admin.html');
            // }, 1200);
            // } else {
            //     Swal.fire({
            //         type: 'success',
            //         title: 'Berhasil Login!',
            //         //text: 'Something went wrong!',
            //         timer: 1500,
            //         //footer: '<a href>Why do I have this issue?</a>'
            //     })
            //     setTimeout(function () {
            //         window.location.replace('/buat-keluhan');
            //     }, 1200);
            // }

        } catch (error) {
            console.log(error)
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'NIM/NIDN atau password Salah!',
                //footer: '<a href>Why do I have this issue?</a>'
            })
        }
        // $.ajax({
        //     method: 'POST',
        //     url: 'http://127.0.0.1:8000/api/auth/login',
        //     data,
        //     headers: {
        //         'Content-type': 'multipart/form-data',
        //     }
        // })
        //     .done(function (response) {
        //         // alert(response.access_token);
        //         // var cookies = new Cookies();
        //         // cookies.set('myCat', response.access_token, { expires: 1 });
        //         // console.log(cookies.get('myCat')); // Pacman
        //         // console.log(response);

        //     })
        //     .fail(function (error) {

        //     })
    });
    */
</script>

</html>