<!DOCTYPE html>
@include('templates.head')
@include('templates.header')
              
      <div class="berita">
        <h1> Berita</h1>
        <hr>

        <div class="row">
            <div class="col-sm-3 thumbnail-berita">
              <img src="image/esport.jpg">
            </div>
            <div class="col-sm-8 berita">
              <a href="Detail berita.html">Wind junior menyabet penghargaan sebagai best team</a>
              <p class="rangkuman-berita"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                  positif melawan Madura United yang menghuni papan atas klasemen Liga 1 2019. Meski 
                  bertindak sebagai Tim tuan rumah, Persib sedikit diunggulkan untuk meraih hasil positif 
                  pada laga ini.</p>

                  <p class="more-info"> <a href="{{route('detailberita')}}">More Info..</a></p>
            </div>
          </div>

          <div class="row">
              <div class="col-sm-3 thumbnail-berita">
                <img src="image/esport.jpg">
              </div>
              <div class="col-sm-8 berita">
                <a href="Detail berita.html">Wind junior menyabet penghargaan sebagai best team</a>
                <p class="rangkuman-berita"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                    positif melawan Madura United yang menghuni papan atas klasemen Liga 1 2019. Meski 
                    bertindak sebagai Tim tuan rumah, Persib sedikit diunggulkan untuk meraih hasil positif 
                    pada laga ini.</p>
  
                    <p class="more-info"> <a href="{{route('detailberita')}}">More Info..</a></p>
              </div>
            </div>

            <div class="row">
                <div class="col-sm-3 thumbnail-berita">
                  <img src="image/esport.jpg">
                </div>
                <div class="col-sm-8 berita">
                  <a href="Detail berita.html">Wind junior menyabet penghargaan sebagai best team</a>
                  <p class="rangkuman-berita"> Laga ini terbilang big match karena mempertemukan Persib yang tengah dalam tren 
                      positif melawan Madura United yang menghuni papan atas klasemen Liga 1 2019. Meski 
                      bertindak sebagai Tim tuan rumah, Persib sedikit diunggulkan untuk meraih hasil positif 
                      pada laga ini.</p>
    
                      <p class="more-info"> <a href="{{route('detailberita')}}">More Info..</a></p>
                </div>
              </div>

        </div>
      </div>
      <nav aria-label="...">
          <ul class="pagination">
            <li class="page-item disabled">
              <a class="page-link" href="#" tabindex="-1">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active">
              <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#">Next</a>
            </li>
          </ul>
        </nav>
      <br>
      <br>
      <br>

@include('templates.footer')
@include('templates.foot')
</html>